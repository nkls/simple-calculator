<?php

namespace App\Services\Types;

use App\Services\Contracts\TypeInterface;

class MultiplicationType implements TypeInterface
{
    public function calc($val1, $val2): float
    {
        return floatval($val1) * floatval($val2);
    }
}
