<?php

namespace App\Services\Contracts;

interface TypeInterface
{
    public function calc($val1, $val2);
}
