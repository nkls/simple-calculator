<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
          integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

    <title>Simple Calculator</title>
</head>
<body class="d-flex flex-column h-100">

<!-- Begin page content -->
<main role="main" class="flex-shrink-0">
    <div class="container">
        <h1 class="mt-5">Simple Calculator</h1>
        <h3 id="error" class="mt-5"></h3>
        <div id="action" class="pl-3"></div>
        <form class="form-inline" method="post" id="calc">
            <input type="hidden" name="type" value="" id="type">
            <input type="hidden" name="val1" id="val1">
            <input type="hidden" name="val2" id="val2">
            <div class="form-group mx-sm-3 mb-2">
                <label for="inputValue" class="sr-only">Value</label>
                <input type="number" class="form-control" id="inputValue">
            </div>
            <button type="button" value="plus" class="type btn btn-primary ml-2">+</button>
            <button type="button" value="minus" class="type btn btn-primary ml-2">-</button>
            <button type="button" value="multiplication" class="type btn btn-primary ml-2">*</button>
            <button type="button" value="division" class="type btn btn-primary ml-2">/</button>
            <button type="submit" class="btn btn-primary ml-2">=</button>
            <button type="reset" class="btn btn-secondary ml-2">Reset</button>
        </form>
    </div>
</main>

<!-- jQuery and Bootstrap Bundle (includes Popper) -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function () {
        let $calc = $('#calc'),
            $error = $('#error'),
            $action = $('#action'),
            $input = $('#inputValue'),
            $type = $('#type'),
            $typeButton = $('.type'),
            $val1 = $('#val1'),
            $val2 = $('#val2'),
            fn = {
                set: function ($e, v) {
                    $e.val(v);
                },
                getAction: function () {
                    let type = '';
                    $typeButton.each(function () {
                        let $item = $(this);
                        if ($item.attr('value') === $type.val()) {
                            type = $item.text();
                        }
                    })
                    $action.text($val1.val() + ' ' + type + ' ' + $val2.val());
                },
                setValue: function () {
                    if ($input.val() === '') {
                        return;
                    }

                    if ($val1.val() === '') {
                        fn.set($val1, $input.val());
                    } else if ($val2.val() === '') {
                        fn.set($val2, $input.val());
                    }

                    fn.set($input, '');
                },
                setType: function ($element) {
                    fn.setValue();
                    if ($val1.val() !== '' && $val2.val() === '') {
                        fn.set($type, $element.attr('value'));
                    }
                    fn.getAction();
                },
                unsetValues: function () {
                    $val1.val('');
                    $val2.val('');
                    $type.val('');
                },
                reset: function () {
                    fn.unsetValues();
                    $error.text('');
                    $action.text('');
                }
            };
        $calc.submit(function (e) {
            e.preventDefault();
            fn.setValue();
            $.ajax({
                url: '/calculator',
                type: 'post',
                dataType: 'json',
                data: $calc.serialize(),
                success: function (data) {
                    $error.text('');
                    fn.getAction();
                    fn.unsetValues();
                    fn.set($input, data);
                    fn.set($error, '');
                },
                error: function (e) {
                    $error.text(e.responseText);
                }
            });
        });
        $calc.on('reset', function () {
            fn.reset();
        });
        $typeButton.click(function () {
            fn.setType($(this));
        });
        $(document).on('keypress', function (){
            $input.focus();
        });
    });
</script>
</body>
</html>
