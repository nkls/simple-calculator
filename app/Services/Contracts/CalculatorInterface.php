<?php

namespace App\Services\Contracts;

interface CalculatorInterface
{
    public function calc(?string $type, $val1, $val2);
}
