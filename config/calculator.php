<?php

return [
    'types' => [
        'plus' => \App\Services\Types\PlusType::class,
        'minus' => \App\Services\Types\MinusType::class,
        'multiplication' => \App\Services\Types\MultiplicationType::class,
        'division' => \App\Services\Types\DivisionType::class,
    ]
];
