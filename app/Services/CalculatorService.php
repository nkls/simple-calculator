<?php

namespace App\Services;

use App\Services\Contracts\CalculatorInterface;
use App\Services\Contracts\TypeInterface;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CalculatorService implements CalculatorInterface
{
    public function calc(?string $type, $val1, $val2): string
    {
        if (!$type) {
            throw new \Exception("Unknown calculator type.");
        }
        return (string)$this->getType($type)
            ->calc($val1, $val2);
    }

    protected function getType(string $type): TypeInterface
    {
        if (
            !$class = Arr::get(
                config('calculator.types'),
                Str::lower($type)
            )
        ) {
            throw new \Exception("Unknown calculator type '{$type}'.");
        }

        return app($class);
    }
}
