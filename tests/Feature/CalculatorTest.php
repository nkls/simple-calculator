<?php

namespace Tests\Feature;

use Tests\TestCase;

class CalculatorTest extends TestCase
{
    public function test_index()
    {
        $response = $this->get('/calculator');

        $response->assertStatus(200);
        $response->assertSeeText('Simple Calculator');
    }

    /**
     * @dataProvider dataProvider
     *
     * @param array $data
     * @param string $expected
     * @param string $message
     */
    public function test_result(array $data, string $expected, string $message)
    {
        $response = $this->post('/calculator', $data);

        $response->assertStatus(200);
        $this->assertEquals($expected, $response->content(), $message);
    }

    public function dataProvider(): array
    {
        return [
            [
                'data' => ['type' => 'plus', 'val1' => 1, 'val2' => 1],
                'expected' => '2',
                'message' => '1 + 1',
            ],
            [
                'data' => ['type' => 'minus', 'val1' => 2, 'val2' => 2],
                'expected' => '0',
                'message' => '2 - 2',
            ],
            [
                'data' => ['type' => 'minus', 'val1' => 2, 'val2' => 3],
                'expected' => '-1',
                'message' => '2 - 3',
            ],
            [
                'data' => ['type' => 'multiplication', 'val1' => 2, 'val2' => 3],
                'expected' => '6',
                'message' => '2 * 3',
            ],
            [
                'data' => ['type' => 'division', 'val1' => 2, 'val2' => 2],
                'expected' => '1',
                'message' => '2 / 2',
            ],
            [
                'data' => ['type' => 'division', 'val1' => 2, 'val2' => 0],
                'expected' => 'Error: Division by zero',
                'message' => '2 / 0',
            ],
            [
                'data' => ['type' => 'DIVISION', 'val1' => 2, 'val2' => 2],
                'expected' => '1',
                'message' => 'Format of the name of the type  2 / 2',
            ],
            [
                'data' => ['type' => null, 'val1' => null, 'val2' => null],
                'expected' => 'Error: Unknown calculator type.',
                'message' => 'type is null',
            ],
            [
                'data' => ['type' => 'unknown', 'val1' => null, 'val2' => null],
                'expected' => "Error: Unknown calculator type 'unknown'.",
                'message' => 'Unknown type',
            ],
        ];
    }
}
