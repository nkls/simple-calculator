<?php

namespace App\Http\Controllers;

use App\Services\CalculatorService;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class CalculatorController extends BaseController
{
    public function index()
    {
        return view('calculator');
    }

    function result(Request $request)
    {
        try {
            $result = app(CalculatorService::class)
                ->calc(
                    $request->input('type'),
                    $request->input('val1'),
                    $request->input('val2')
                );
        } catch (\Exception $e) {
            $result = sprintf('Error: %s', $e->getMessage());
        }

        return response($result, 200);
    }
}
